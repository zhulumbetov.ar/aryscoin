import sqlite3
import hashlib
from hashlib import sha256
import time


def updatehash(*args):
    hashing_text = "".join(map(str, args))
    return sha256(hashing_text.encode('utf-8')).hexdigest()

class Block():
    def __init__(self, index, previous_hash, data, timestamp, nonce=0):
        self.index = index
        self.previous_hash = previous_hash
        self.data = data
        self.timestamp = timestamp
        self.nonce = nonce
        self.hash = self.calculate_hash()

    def calculate_hash(self):
        return updatehash(self.index, self.previous_hash, self.data, self.timestamp, self.nonce)

class MerkleTree:
    def __init__(self):
        self.transactions = []

    def add_transaction(self, transaction_data):
        self.transactions.append(transaction_data)

    def build_tree(self):
       tree = [hashlib.sha256(data.encode('utf-8')).hexdigest() for data in self.transactions]

       while len(tree) > 1:
           tree = [hashlib.sha256((tree[i] + tree[i + 1]).encode('utf-8')).hexdigest() for i in range(0, len(tree), 2)]

       return tree[0]

    def verify_root_hash(self, root_hash):
        return self.build_tree() == root_hash

class Blockchain():
    def __init__(self):
        self.chain = []
        self.merkle_tree = MerkleTree() 
        self.create_genesis_block()

    def create_genesis_block(self):
        genesis_block = Block(0, "0", "Genesis Block", int(time.time()))
        self.chain.append(genesis_block)
        self.merkle_tree.add_transaction("Genesis Block")

    def add_block(self, new_block):
        new_block.previous_hash = self.get_latest_block().hash
        new_block.hash = new_block.calculate_hash()
        self.chain.append(new_block)
        self.merkle_tree.add_transaction(new_block.data)

    def get_latest_block(self):
        return self.chain[-1]

    def is_valid_blockchain(self):
        for i in range(1, len(self.chain)):
            current_block = self.chain[i]
            previous_block = self.chain[i - 1]

            if current_block.hash != current_block.calculate_hash():
                return False

            if current_block.previous_hash != previous_block.hash:
                return False

        return True

conn = sqlite3.connect('blockchain.db')
cursor = conn.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS transactions
                  (sender TEXT, recipient TEXT, amount REAL)''')
conn.commit()


blockchain = Blockchain()

def add_transaction(sender, recipient, amount, transaction_type=None):
    if transaction_type:
        transaction_data = f"{transaction_type}{amount}"
    else:
        transaction_data = f"{sender}{recipient}{amount}"
    
    cursor.execute("INSERT INTO transactions (sender, recipient, amount) VALUES (?, ?, ?)", (sender, recipient, amount))
    conn.commit()
    blockchain.merkle_tree.add_transaction(transaction_data)


def get_balance(wallet_address):
    cursor.execute("SELECT SUM(amount) FROM transactions WHERE sender = ? OR recipient = ?", (wallet_address, wallet_address))
    result = cursor.fetchone()
    if result[0] is None:
        return 0
    else:
        return result[0]