import sqlite3
from blockchain.blockchain import Blockchain, add_transaction, Block
from blockchain.wallet import Wallet
import time


class Wallet():
    def __init__(self, name):
        self.name = name
        self.balance = 0

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        if self.balance >= amount:
            self.balance -= amount
            return True
        else:
            return False

def send_money(sender_wallet, recipient_wallet, amount):
    if sender_wallet and recipient_wallet and sender_wallet.withdraw(amount):
        recipient_wallet.deposit(amount)
        return True
    return False

def show_all_balances(cursor, wallets):
    print("Balances of All People:")
    for wallet_name, wallet in wallets.items():
        balance = wallet.balance
        print(f"{wallet_name}: {balance} tokens")

conn = sqlite3.connect('blockchain.db')
cursor = conn.cursor()

blockchain = Blockchain()

wallets = {
    "Alice": Wallet("Alice"),
    "Bob": Wallet("Bob"),
    "Charlie": Wallet("Charlie"),
}

while True:
    print("\nOptions:")
    print("1: View Balance")
    print("2: Send Money")
    print("3: Mine Block")
    print("4: Show Blockchain")
    print("5: Show Balances of All People")
    print("6: Check Blockchain Validity")
    print("7: Exit")
    choice = input("Enter your choice: ")

    if choice == "1":
        wallet_name = input("Enter wallet name to view balance: ")
        wallet = wallets.get(wallet_name)

        if wallet:
            balance = wallet.balance
            print(f"Balance of {wallet_name}: {balance} tokens")
        else:
            print("Invalid wallet name.")

    elif choice == "2":
        sender_name = input("Enter your wallet name: ")
        recipient_name = input("Enter recipient wallet name: ")
        amount = float(input("Enter amount: "))

        sender_wallet = wallets.get(sender_name)
        recipient_wallet = wallets.get(recipient_name)

        if sender_wallet and recipient_wallet and send_money(sender_wallet, recipient_wallet, amount):

            add_transaction(sender_name, recipient_name, amount)
            print("Transaction Successful!")
        else:
            print("Transaction Failed. Insufficient balance or invalid wallet name.")

    elif choice == "3":
        miner_reward = 1.0
        wallet_name = input("Enter your wallet name for mining: ")

        wallet = wallets.get(wallet_name)

        if wallet:
            wallet.deposit(miner_reward)
            add_transaction("Mining Reward", wallet_name, miner_reward)

            last_block = blockchain.get_latest_block()
            new_block = Block(last_block.index + 1, last_block.hash, f"Block {last_block.index + 1}", int(time.time()))
            blockchain.add_block(new_block)
            print("Block mined!")
        else:
            print("Mining Failed. Invalid wallet name.")

    elif choice == "4":
        for block in blockchain.chain:
            print(block.__dict__)

    elif choice == "5":
        show_all_balances(cursor, wallets)

    elif choice == "6":
        if blockchain.is_valid_blockchain():
            print("Blockchain is valid.")
        else:
            print("Blockchain is not valid.")

    elif choice == "7":
        conn.close()
        break
